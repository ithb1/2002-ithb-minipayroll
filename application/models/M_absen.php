<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class M_absen extends MY_Model
{

    var $table_name = "absen";
    var $pk = "";
//    var $fk = "fid";



    function getAllWithUser() {
        $query = $this->db->query("
                                SELECT
                                    abs.*, usr.noinduk, usr.fullname
                                FROM
                                    absen abs
                                    LEFT JOIN
                                    user usr
                                    ON abs.id_no = usr.userid
                            ");
        return $query->result();
    }
}