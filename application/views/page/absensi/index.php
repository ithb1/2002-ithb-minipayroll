
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
     Absensi
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=site_url('');?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href=""> Absensi </a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">
            Tabel Data
          </h3>
            <div class="pull-right">
                <?=form_open("http://localhost/rachman/payroll/Integrasi/absen", "style=display:inline;");?>
                <?=form_hidden("data",json_encode( $rowData ));?>
                <?=form_submit("btnsubmit", "PUSH","class='btn btn-success btn-xs'");?>
                <?=form_close();?>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>NIK</th>
              <th>Nama</th>
              <th>Tanggal</th>
              <th>Jam Masuk</th>
              <th>Jam Keluar</th>
            </tr>
            </thead>
            <tbody><?php
            $no = 1;
            foreach ($rowData as $row) :
            ?>
            <tr>
                <td><?=$no++?></td>
                <td> <?=$row->noinduk?> </td>
                <td> <?=$row->fullname?> </td>
                <td align="center"> <?=$row->tanggal?> </td>
                <td align="center"> <?=$row->jam_masuk?> </td>
                <td align="center"> <?=$row->jam_keluar?> </td>
            </tr>
            <?php endforeach;?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->






<!-- Modal -->
<div id="modalForm" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Kategori</h4>
      </div>
      <?=form_open("Absensi/add","class='form-horizontal'");
      ?>
      <div class="modal-body">

          <div class="box-body">
            <div class="row">
              <div class="col-md-12">            
                <div class="form-group">
                  <label for="noinduk" class="col-sm-4 control-label">Nama</label>
                  <div class="col-sm-8">
                    <input type="hidden" class="form-control" id="id" placeholder="id" name="id" value="">
                      <select name="noinduk" id="noinduk" required class="form-control">
                          <option value="">- karyawan -</option>
                          <?php foreach($rowUser as $row):?>
                              <option value="<?=$row->noinduk?>" ><?=$row->noinduk." - ".$row->fullname?></option>
                          <?php endforeach;?>
                      </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="tanggal" class="col-sm-4 control-label">Karyawan</label>
                  <div class="col-sm-8">
                    <input type="date" class="form-control" id="tanggal" placeholder="tanggal" name="tanggal" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="jam_masuk" class="col-sm-4 control-label">Jam Masuk</label>
                  <div class="col-sm-8">
                    <input type="time" class="form-control" id="jam_masuk" placeholder="jam_masuk" name="jam_masuk" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="jam_keluar" class="col-sm-4 control-label">Jam Keluar</label>
                  <div class="col-sm-8">
                    <input type="time" class="form-control" id="jam_keluar" placeholder="jam_keluar" name="jam_keluar" value="">
                  </div>
                </div>
              </div>        
            </div>
          </div>
          <!-- /.box-footer -->
      </div>
      <div class="modal-footer">
        <?=form_submit("btnsubmit", "save","class='btn btn-success'");?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      <?=form_close();?>
    </div>

  </div>
</div>


<script>
  function getDetail(ini) {
    var id = $(ini).attr('data-id');
    $.ajax({
      type: 'GET',
      url: "<?=base_url('');?>Absen/detail/"+id,
      success: function (data) {
        //Do stuff with the JSON data
          console.log(data);
         $('#id').val(id).hide();
         $('#noinduk').val(data.noinduk);
        }
    });
  }

  function clearForm() {    
     $('#id').val("");
     $('#noinduk').val("");
  }
</script>