<?php
    $iduser = $this->session->userdata("id");
    $user = $this->M_user->getDetail($iduser);
?>
<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
  <li class="header">MAIN NAVIGATION</li>
    <li class="<?=($this->uri->segment(1) == 'User')?'active':''?>"><a href="<?=site_url('User')?>"><i class="fa fa-users"></i> <span>User</span></a></li>
    <li class="<?=($this->uri->segment(1) == 'Absensi')?'active':''?>"><a href="<?=site_url('Absensi')?>"><i class="fa fa-calendar"></i> <span>Absensi</span></a></li>
</ul>