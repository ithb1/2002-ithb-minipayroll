<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absensi extends CI_Controller {
	
	var $kelas = "Absensi";

	function __construct(){
		parent::__construct();
        if (!$this->session->userdata("id")){
            redirect("Welcome");
        }
        $id = $this->session->userdata("id");
        $this->user = $this->M_user->getDetail($id);
	}

    public function index($isIntegrasi = 0){
		$data["rowData"] = $this->M_absen->getAllWithUser();
		$data["rowUser"] = $this->M_user->getAll();
		$data['konten'] = "absensi/index";
        if($isIntegrasi) "warning";
		$this->load->view('template',$data);
	}

	public function lembur(){
//		$data["rowData"] = $this->M_absen->getAll();
		$data["rowData"] = [];
		$data['konten'] = "master/jabatan/lembur";
		$this->load->view('template',$data);
	}

	public function detail($id){
	    header('Content-Type: application/json');
		$rowData = $this->M_absen->getDetail($id);
	    echo json_encode( $rowData );
	}

	public function add(){
		$id = $this->input->post("id");
		$data["noinduk"] = $noinduk = $this->input->post("noinduk");
		$data["tanggal"] = $tanggal = $this->input->post("tanggal");
		$data["jam_masuk"] = $jam_masuk = $this->input->post("jam_masuk");
		$data["jam_keluar"] = $jam_keluar = $this->input->post("jam_keluar");

		if($id) {
            $this->M_absen->update($id,$data);
//            $this->jejak->add($this->user->userid, "Mengubah Jurusan ".$data['nama'], "Jurusan/index");
        }
		else {
            $cek = $this->M_absen->getDetailBy("noinduk = '$noinduk' AND tanggal = '$tanggal' AND jam_masuk = '$jam_masuk' AND jam_keluar = '$jam_keluar'");

            if(!$cek) $this->M_absen->add($data);
//            $this->jejak->add($this->user->userid, "Menambah Jurusan ".$data['nama'], "Jurusan/index");
        }

		redirect($this->kelas);
	}

	public function delete($id){
        $jurusan = $this->M_absen->getDetail($id);
        $this->M_absen->delete($id);
        $this->jejak->add($this->user->userid, "Menghapus Jurusan ".$jurusan->nama, "Jurusan/index");
        redirect($this->kelas);
	}

//    public function push(){
//        $rowData = $this->M_absen->getAll();
//        $jsonData = json_encode( $rowData );
//        redirect("http://localhost/rachman/payroll/Integrasi/absen?data=".$jsonData);
////        redirect("http://www.samaxelunicorn.com/Integrasi/absen?data=".$jsonData);
//    }
}
